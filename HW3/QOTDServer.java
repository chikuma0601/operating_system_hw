/*
 * This program is the modified version of DataServer.java for programming problems 3.16
 * Quote of the day service(QOTD)
 */
import java.net.*;
import java.io.*;
import java.util.concurrent.ThreadLocalRandom;

public class QOTDServer {
	public static void main(String [] args) {

		final String [] quotes = {
			"I ask you: Do you want total war?\nIf necessary, do you want a war more total and radical than anything that we can even yet imagine?",
			"We are not a charitable institution but a Party of revolutionary socialists.",
			"ARBEIT MACHT FREI",
			"Write tests before write codes.",
			"The Flying Spaghetti Monster is real! Press [F] to pay respect.",
			"I am the Flying Spaghetti Monster.\nThou shalt have no other monsters before Me (Afterwards is OK; just use protection).\nThe only Monster who deserves capitalization is Me!\nOther monsters are false monsters, undeserving of capitalization.\n— Suggestions 1:1",
			"--INSERT YOUR MEME HERE--"
		};

		try {
			ServerSocket sock = new ServerSocket(6013);

			/* Listen for connections */
			while (true) {
				/* Get a random quote */
				int randomQuoteIndex = ThreadLocalRandom.current().nextInt(0, quotes.length);

				Socket client = sock.accept();
				PrintWriter pout = new PrintWriter(client.getOutputStream(), true);

				pout.println(quotes[randomQuoteIndex]);

				client.close();
			}
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
	}
}