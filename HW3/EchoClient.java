import java.util.Scanner;
import java.net.*;
import java.io.*;

public class EchoClient {
	public static void main (String [] args) {
		try {
			/* Make connection to local server socket */
			Socket sock = new Socket("127.0.0.1", 6013);
			
			String msg;
			Scanner sc = new Scanner(System.in);
			System.out.print("Input messages to send: ");
			msg = sc.nextLine();

			InputStream in = sock.getInputStream();
			PrintWriter pout = new PrintWriter(sock.getOutputStream(), true);
			BufferedReader bin = new BufferedReader(new InputStreamReader(in));

			pout.println(msg);

			String line;
			while ((line = bin.readLine()) != null) {
				System.out.println(line);
			}

			sock.close();

		} catch (IOException ioe) {
			System.err.println(ioe);
		}
	}
}