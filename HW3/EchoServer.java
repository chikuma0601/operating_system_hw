import java.net.*;
import java.io.*;

public class EchoServer {
	public static void main (String [] args) {
		try {
			ServerSocket sock = new ServerSocket(6013);

			/* Listen for connections */
			while (true) {
				Socket client = sock.accept();
				InputStream in = client.getInputStream();
				byte [] msgBuffer = new byte [256];

				/* If nothing could be read: client closed connection */
				if (in.read(msgBuffer) == -1) {
					System.out.println("Client has close end of socket connection.");
					client.close();
					break;
				}
				else {
					PrintWriter pout = new PrintWriter(client.getOutputStream(), true);
					pout.println(msgBuffer);
				}

				client.close();
			}
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
	}
}

